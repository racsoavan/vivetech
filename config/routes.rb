Rails.application.routes.draw do
  get '/products/:id', to: 'dashboard#show'
  namespace :admin do
    get "/" => "dashboard#index"
    get 'dashboard/index'
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :products, only: [:index, :create]
    end
  end
  
  root to: 'dashboard#index' 
end
