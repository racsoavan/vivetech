class CreateLoadHistories < ActiveRecord::Migration[6.1]
  def change
    create_table :load_histories, id: :uuid do |t|
      t.text   :description
      t.integer :products, default: 0
      t.integer :failed, default: 0
      t.integer :status, default: 0
      t.timestamps
    end
  end
end
