class CreateVariants < ActiveRecord::Migration[6.1]
  def change
    create_table :variants, id: :uuid do |t|
      t.string   :name, null: false
      t.float :price, null: false
      t.uuid :product_id
      t.timestamps
    end
  end
end
