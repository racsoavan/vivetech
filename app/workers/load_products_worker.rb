class LoadProductsWorker 
    include Sidekiq::Worker
    sidekiq_options retry: false
  
    def perform(lh_id)
        lh = LoadHistory.find lh_id
        product_failed = 0
        JSON.parse(lh.description).each do |product|
            if product["variants"].nil?
                product_failed += 1
            else
                begin
                    variants = product["variants"] 
                    new_product = Product.new
                    new_product.name = product["name"]
                    new_product.description = product["description"]
                    variants.map{|variant| new_product.variants.build(variant)}
                    product_failed += 1 unless new_product.save
                rescue => exception
                    p "VALIDA: #{exception}"
                    product_failed += 1
                end
            end
        end
        lh.status = 1
        lh.failed = product_failed
        lh.save
    end
end