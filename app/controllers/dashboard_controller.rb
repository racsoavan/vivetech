class DashboardController < ApplicationController

    def index
        @product_count = Product.count
        @products = Product.page(params[:page])
    end
    def show
        @product = Product.find params[:id]
    end
end
