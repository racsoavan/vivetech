module Api
    module V1
      class ProductsController < ApplicationController
        skip_before_action :verify_authenticity_token
        def index
            render json: {status: 200, message: 'Listo API'}
        end
        def create
            msg = 'Petición Registrada'
            code = 200
            data = request.body.read()
            count_products =  JSON.parse(data).count
            p "#{count_products}"
            if count_products == 0 || count_products > 10_000
                msg = 'la cantidad de productos debe ser entre 1 - 10000'
                code = 406
            else
                lh = LoadHistory.create(description: data, products:count_products)
                LoadProductsWorker.perform_async(lh.id)
            end
            render json: {status: 204, message: msg}
        end
    end
  end
end
