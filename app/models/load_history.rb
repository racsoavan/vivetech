class LoadHistory < ApplicationRecord
    default_scope { order(created_at: :asc) }
	enum status: [ :pendiente, :procesado ]
end
