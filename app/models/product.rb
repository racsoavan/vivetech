class Product < ApplicationRecord
    paginates_per 50
    validates :variants, :length => { :minimum => 1 }    
    has_many :variants
end
